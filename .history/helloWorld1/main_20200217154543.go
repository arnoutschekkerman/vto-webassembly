package main

import (
	"syscall/js"
)

func main() {
	window := js.Global()
	doc := window.Get("document")
	hello := doc.Call("getElementById", "hello")
	hello.Set("innerHTML", "Hello WASM!")
}
