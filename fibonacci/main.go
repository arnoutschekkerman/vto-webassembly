package main

import (
	"strconv"
	"syscall/js"
	"time"
)

func Fibonacci(limit int) string {
	var numbers string
	for i := 0; i < limit; i++ {
		numbers += (strconv.Itoa(FibonacciRecursion(i)) + " ")
	}
	return numbers
}

func FibonacciRecursion(n int) int {
	if n <= 1 {
		return n
	}
	return FibonacciRecursion(n-1) + FibonacciRecursion(n-2)
}

func main() {
	window := js.Global()
	doc := window.Get("document")
	hello := doc.Call("getElementById", "hello")

	t := time.Now()
	fib := Fibonacci(41)
	duration := time.Since(t).Milliseconds()

	hello.Set("innerHTML", (fib + "<br/> " + strconv.Itoa(int(duration)) + "ms"))
}
